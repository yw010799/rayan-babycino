//testing to see if y is assigned to x


class TestBugJ2 {
    public static void main(String[] a) {
	System.out.println("x+=y, then x is returned. Initially, x is set to 2, y is set to 3. Correct output is 5. If output is 3, that means y was assigned to x (bug)");
        System.out.println(new Test().f());
    }
}

class Test {

    public int f(){
        int x=2;
        int y=3;
        x += y;
        return x;
    }

}

