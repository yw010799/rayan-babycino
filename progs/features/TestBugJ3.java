//testing to see if x is modified at all


class TestBugJ3 {
    public static void main(String[] a) {
        System.out.println("x+=y, then x is returned after. Initially, x is set to 2, y is set to 3. Correct output is 5. If output is 2, that means x wasn't modified at all (bug)");
        System.out.println(new Test().f());
    }
}

class Test {

    public int f(){
        int x=2;
        int y=3;
        x += y;
        return x;
    }

}

